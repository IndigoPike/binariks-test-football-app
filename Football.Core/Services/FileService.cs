﻿using Football.Core.Data;
using Football.Core.Data.GitHub;
using Football.Core.Interfases;
using Football.Core.Managers;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Football.Core.Services
{
    public class FileService : IFileService
    {
        private const string pattern = "\\w+.\\d.json";

        private GitHubManager _gitHubManager;
        private DownloadManager _downloadManager;
        private StatisticsManager _statisticsManager;

        public FileService()
        {
            _gitHubManager = new GitHubManager();
            _downloadManager = new DownloadManager();
            _statisticsManager = new StatisticsManager();
        }
        public async Task<List<GitFileInfo>> GetFilesInfo()
        {
            var allFiles = await _gitHubManager.GitFileInfosAsync();
            var regex = new Regex(pattern); // filter cubs and clubs files
            return allFiles.Where(x => regex.IsMatch(x.Name))
                           .ToList();
        }

        public async Task<LeaguesStatistics> GetLeaguesStatistics(IEnumerable<string> links)
        {
            var jsonList = await _downloadManager.DownloadContentList(links);
            return _statisticsManager.GetLeaguesStatistics(jsonList);
        }

        public async Task<LeaguesStatistics> GetLeaguesStatistics(IEnumerable<IFormFile> files)
        {
            return await _statisticsManager.GetLeaguesStatistics(files);
        }

        public bool ValidateFiles(IEnumerable<IFormFile> files, out string message)
        {
            message = string.Empty;
            var regex = new Regex(pattern);
            foreach (var file in files)       
            {
                if (!regex.IsMatch(file.FileName))
                {
                    message = $"{file.FileName} - not supported extention";
                    return false;
                }
            }
            return true;
        }
    }
}

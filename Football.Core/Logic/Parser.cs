﻿using Football.Core.Data;
using Football.Core.Data.GitHub;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Football.Core.Logic
{
    public class Parser
    {
        public static League ParseLeague(string json) =>
            JsonConvert.DeserializeObject<League>(json);

        public static List<GitFileInfo> ParseGitFileInfoList(string json) =>
            JsonConvert.DeserializeObject<List<GitFileInfo>>(json);
    }
}

﻿using System;

namespace Football.Web.Models
{
    public class MatchViewModel
    {
        public string Round { get; set; }
        public DateTime Date { get; set; }
        public string Team1 { get; set; }
        public string Team2 { get; set; }
        public int Team1Goals { get; set; }
        public int Team2Goals { get; set; }
        public int GeneralGoals { get; set; }
    }
}

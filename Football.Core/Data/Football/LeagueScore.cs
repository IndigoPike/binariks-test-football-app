﻿namespace Football.Core.Data
{
    public class LeagueScore
    {
        public LeagueScore(string name, CommandScore attackers, CommandScore defenders, CommandScore bestRatio)
        {
            Name = name;
            Attackers = attackers;
            Defenders = defenders;
            BestRatio = bestRatio;
        }

        public string Name { get; private set; }
        public CommandScore Attackers { get; private set; }
        public CommandScore Defenders { get; private set; }
        public CommandScore BestRatio { get; private set; }
    }
}

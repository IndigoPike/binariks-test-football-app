﻿using Football.Core.Data;
using Football.Core.Data.GitHub;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Football.Core.Interfases
{
    public interface IFileService
    {
        Task<List<GitFileInfo>> GetFilesInfo();

        Task<LeaguesStatistics> GetLeaguesStatistics(IEnumerable<string> links);

        Task<LeaguesStatistics> GetLeaguesStatistics(IEnumerable<IFormFile> files);

        bool ValidateFiles(IEnumerable<IFormFile> files, out string message);
    }
}

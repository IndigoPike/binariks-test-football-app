﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace Football.Core.Data
{
    public class Match
    {
        [JsonProperty("round")]
        public string Round { get; set; }
        [JsonProperty("date")]
        public DateTime Date { get; set; }
        [JsonProperty("team1")]
        public string Team1 { get; set; }
        [JsonProperty("team2")]
        public string Team2 { get; set; }
        [JsonProperty("score", NullValueHandling = NullValueHandling.Ignore)]
        public Score Score { get; set; }

        [JsonIgnore]
        public int Team1Goals => Score.Ft[0];

        [JsonIgnore]
        public int Team2Goals => Score.Ft[1];

        [JsonIgnore]
        public int GeneralGoals => Score.Ft.Sum();
    }

    public class Score
    {
        /// <summary>
        /// Ft[0] - Team1, Ft[1] - Team2
        /// </summary>
        [JsonProperty("ft")]
        public int[] Ft { get; set; }
    }
}

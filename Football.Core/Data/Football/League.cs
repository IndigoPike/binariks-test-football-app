﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Football.Core.Data
{
    public class League
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("matches")]
        public List<Match> Matches { get; set; }
    }
}

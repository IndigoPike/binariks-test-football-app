﻿using Football.Core.Data;
using Football.Core.Data.GitHub;
using Football.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace Football.Web.Extentions
{
    public static class ViewModelConvertors
    {
        public static LeaguesStatisticsViewModel ToViewModel(this LeaguesStatistics value)
            => new LeaguesStatisticsViewModel(value.Leagues.ToViewModel(), value.ProductiveDays.ToViewModel());

        public static List<MatchViewModel> ToViewModel(this IEnumerable<Match> value)
            => value.Select(x => new MatchViewModel
            {
                Date = x.Date,
                GeneralGoals = x.GeneralGoals,
                Round = x.Round,
                Team1 = x.Team1,
                Team1Goals = x.Team1Goals,
                Team2 = x.Team2,
                Team2Goals = x.Team2Goals
            }).ToList();

        public static List<LeagueScoreViewModel> ToViewModel(this IEnumerable<LeagueScore> value)
            => value.Select(x => new LeagueScoreViewModel(x.Name, x.Attackers.ToViewModel(), x.Defenders.ToViewModel(), x.BestRatio.ToViewModel())).ToList();

        public static CommandScoreViewModel ToViewModel(this CommandScore value) 
            => new CommandScoreViewModel(value.Name, value.GoalsScored, value.MissedGoals);

        public static List<FileViewModel> ToViewModel(this IEnumerable<GitFileInfo> value)
            => value.Select(x => new FileViewModel
            {
                Link = x.Link,
                Name = x.Name
            }).ToList();
    }
}

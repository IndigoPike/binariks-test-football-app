﻿namespace Football.Web.Models
{
    public class FileViewModel
    {
        public string Name { get; set; }

        public string Link { get; set; }
    }
}

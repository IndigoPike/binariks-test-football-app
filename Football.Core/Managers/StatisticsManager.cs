﻿using Football.Core.Data;
using Football.Core.Logic;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Football.Core.Managers
{
    public class StatisticsManager
    {
        public async Task<LeaguesStatistics> GetLeaguesStatistics(IEnumerable<IFormFile> files)
        {
            var legues = new List<League>();

            foreach (var file in files)
            {
                using (var stream = new StreamReader(file.OpenReadStream()))
                {
                    var json = await stream.ReadToEndAsync();
                    var legue = Parser.ParseLeague(json);
                    legues.Add(legue);
                    stream.Close();
                }
            }

            var statistics = Analizer.GetLeaguesStatistic(legues);
            return statistics;
        }

        public LeaguesStatistics GetLeaguesStatistics(IEnumerable<string> jsonList)
        {
            var leagues = jsonList
                .Select(json => Parser.ParseLeague(json))
                .ToList();
            var statistics = Analizer.GetLeaguesStatistic(leagues);
            return statistics;
        }
    }
}

﻿namespace Football.Web.Models
{
    public class CommandScoreViewModel
    {
        public CommandScoreViewModel(string name, int goals, int missed)
        {
            Name = name;
            GoalsScored = goals;
            MissedGoals = missed;
            GoalsMissedRate = goals - missed;
        }

        public string Name { get; private set; }
        public int GoalsScored { get; private set; }
        public int MissedGoals { get; private set; }
        public int GoalsMissedRate { get; private set; }
    }
}
